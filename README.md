# captchaway java HTTP client 

[![build status](https://gitlab.com/serphacker/captchaway/badges/master/pipeline.svg)](https://gitlab.com/serphacker/captchaway/commits/master) 
[![coverage report](https://gitlab.com/serphacker/captchaway/badges/master/coverage.svg)](https://gitlab.com/serphacker/captchaway/commits/master)
[![code quality](https://api.codacy.com/project/badge/Grade/a1b564ef434c41e08af674bbec05b186)](https://www.codacy.com/app/noguespi/captchaway?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=serphacker/captchaway&amp;utm_campaign=Badge_Grade)
[![Maven Central](https://img.shields.io/maven-central/v/com.serphacker.captchaway/captchaway.svg?label=Maven%20Central)](https://search.maven.org/search?q=g:%22com.serphacker.captchaway%22%20AND%20a:%22captchaway%22)


A java lib to solve captchas using multiple online services.

Homepage :  https://gitlab.com/serphacker/captchaway

Issues and bug report : https://gitlab.com/serphacker/captchaway/issues

Features : 

* Solve simple captcha image
* Solve Google recaptcha
* Low footprint, only one dependency : 
    * [FasterXML/jackson](https://github.com/FasterXML/jackson) for json processing
    * [slf4j-api](https://www.slf4j.org/) for logging

Supported services : 

* TODO
 
 ## Install
 
 [![Maven Central](https://img.shields.io/maven-central/v/com.serphacker.captchaway/captchaway.svg?label=Maven%20Central)](https://search.maven.org/search?q=g:%22com.serphacker.captchaway%22%20AND%20a:%22captchaway%22)
 (require java minimal version 11 )

 ```xml
<dependency>
  <groupId>com.serphacker.captchaway</groupId>
  <artifactId>captchaway</artifactId>
  <version>LATEST</version>
</dependency>
```
 
 ## Usage
 
 TODO
 
 ## Build

### Building jar

`mvn clean package`

### Running integration tests

1. run the integration tests with the following maven command : 
`mvn -P integration-test  clean verify`

## License

The MIT License (MIT)
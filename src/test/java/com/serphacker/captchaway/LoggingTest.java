/*
 * CaptchaWay - A java lib to solve captchas using multiple APIs https://gitlab.com/serphacker/captchaway
 *
 * Copyright (c) 2019 SERP Hacker
 * @author Pierre Nogues <support@serphacker.com>
 * @license https://opensource.org/licenses/MIT MIT License
 */

package com.serphacker.captchaway;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggingTest {

    public final static Logger LOG = LoggerFactory.getLogger(LoggingTest.class);

    @Test
    public void helloTest() {
        LOG.info("logging test");
    }

}

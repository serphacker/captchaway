/*
 * CaptchaWay - A java lib to solve captchas using multiple APIs https://gitlab.com/serphacker/captchaway
 *
 * Copyright (c) 2019 SERP Hacker
 * @author Pierre Nogues <support@serphacker.com>
 * @license https://opensource.org/licenses/MIT MIT License
 */

package com.serphacker.captchaway;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class DummyIT {

    // we need to have at least one integration-test to make the build pass
    public void success() {
        assertTrue(true);
    }

}
